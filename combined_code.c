//***************************************************************************************
//  MSP430 Blink the LED Demo - Software Toggle P1.0
//
//  Description; Toggle P1.0 by xor'ing P1.0 inside of a software loop.
//  ACLK = n/a, MCLK = SMCLK = default DCO
//
//                MSP430x5xx
//             -----------------
//         /|\|              XIN|-
//          | |                 |
//          --|RST          XOUT|-
//            |                 |
//            |             P1.0|-->LED
//
//  Texas Instruments, Inc
//  July 2013
//***************************************************************************************
#include "main.h"
#include "driverlib/driverlib.h"
#include "hal_LCD.h"
#include <msp430.h> //might not need it

#define PI 3.141592
#define RADIUS 32.5

#define FORWARD 0
#define BACKWARD 1
#define LEFT 2
#define RIGHT 3

#define pos1 4   /* Digit A1 - L4  */
#define pos2 6   /* Digit A2 - L6  */
#define pos3 8   /* Digit A3 - L8  */
#define pos4 10  /* Digit A4 - L10 */
#define pos5 2   /* Digit A5 - L2  */
#define pos6 18  /* Digit A6 - L18 */

#define INTERVAL_TIME_INIT (32768*10)//10 seconds
#define INTERVAL_TIME_LR (32768/10)//0.1 seconds

char ADCState = 0; //Busy state of the ADC
int16_t ADCResult = 0; //Storage for the ADC conversion result

int forwardKeypadPress;
int backwardKeypadPress;
int rightKeypadPress;
int leftKeypadPress;

int distance;
int distance_array[4];
int distanceFromKeypad;

int timeUp;

void LCD_welcome(){
    displayScrollText("TEAM 4 - WHEELCHAIR IS READY!");
}

//7 is pressed to repeat instructions
void LCD_keyInstructions(){

    displayScrollText("KEYPAD INSTRUCTIONS - PAY ATTENTION");

    displayScrollText("PRESS * TO MOVE LEFT");  //display L
    displayScrollText("PRESS # TO MOVE RIGHT"); //display R
    displayScrollText("PRESS 0* TO ENTER FORWARD DIR MODE");   //diplay F
    displayScrollText("PRESS 0# TO ENTER BACKWARD DIR MODE"); //display B

    // displayScrollText("PRESS 7 TO REPEATE THE INSTRUCTION");

}


//display in pos3, pos4
void keypad_activate_wheel_dir(){

GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN3);//ROW3

    if((GPIO_getInputPinValue(GPIO_PORT_P1, GPIO_PIN4)) == 1){
        //* selected
            // showChar('L', pos3);
            leftKeypadPress = 1;

    }

    else if((GPIO_getInputPinValue(GPIO_PORT_P8, GPIO_PIN3)) == 1)  {
        //0 selected
            // FOR SOME TIME TIGHT POLL THESE TWO KEYS OR
            // USE ANOTHER 0 TO INDICATE DONE

            if((GPIO_getInputPinValue(GPIO_PORT_P1, GPIO_PIN4)) == 1){
        //* selected
                //showChar('F', pos3);
                forwardKeypadPress = 1;

            }

            if((GPIO_getInputPinValue(GPIO_PORT_P5, GPIO_PIN3)) == 1){//char # selected
                //showChar('B', pos3);
                backwardKeypadPress = 1;
            }


        }

    else if((GPIO_getInputPinValue(GPIO_PORT_P5, GPIO_PIN3)) == 1){//char # selected
            // showChar('R', pos3);
            rightKeypadPress = 1;
    }

GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN3);

}


//distance only takes numbers
void keypad_activate_dist(int position, int index){
    // keypad_activate_wheel_dir();

    GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN5);//ROW1;

    if(GPIO_getInputPinValue(GPIO_PORT_P1, GPIO_PIN4) == 1){
            showChar('1', position);
            distance_array[index] = 1;
            //DETECT 1
    }
    else if((GPIO_getInputPinValue(GPIO_PORT_P8, GPIO_PIN3)) == 1){

            showChar('2', position);
            distance_array[index] = 2;
    }

    else if((GPIO_getInputPinValue(GPIO_PORT_P5, GPIO_PIN3)) == 1){
            showChar('3', position);
            distance_array[index] = 3;
    }
    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN5);

    //////////////////////////////////////
    GPIO_setOutputHighOnPin(GPIO_PORT_P5, GPIO_PIN0);//ROW2

    if((GPIO_getInputPinValue(GPIO_PORT_P1, GPIO_PIN4)) == 1){
            showChar('4', position);
            distance_array[index] = 4;
    }
    else if((GPIO_getInputPinValue(GPIO_PORT_P8, GPIO_PIN3)) == 1){
            showChar('5', position);
            distance_array[index] = 5;
    }
    else if((GPIO_getInputPinValue(GPIO_PORT_P5, GPIO_PIN3)) == 1){
            showChar('6', position);
            distance_array[index] = 6;
    }
    GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN0);
    ///////////////////////////////////
    GPIO_setOutputHighOnPin(GPIO_PORT_P5, GPIO_PIN2);//ROW3

    if((GPIO_getInputPinValue(GPIO_PORT_P1, GPIO_PIN4)) == 1){
            showChar('7', position);
            distance_array[index] = 7;
    }
    else if((GPIO_getInputPinValue(GPIO_PORT_P8, GPIO_PIN3)) == 1){
            showChar('8', position);
            distance_array[index] = 8;
    }
    else if((GPIO_getInputPinValue(GPIO_PORT_P5, GPIO_PIN3)) == 1){
            showChar('9', position);
            distance_array[index] = 9;
    }
    GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN2);
    ////////////////////////////////

    GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN3);//row 4 for 0
    if((GPIO_getInputPinValue(GPIO_PORT_P8, GPIO_PIN3)) == 1){
        showChar('0', position);
        distance_array[index] = 0;
    }
    GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN3);

}


void display_yellow_LED(){   //yellow is A3 11

    GPIO_setOutputHighOnPin(GPIO_PORT_P8, GPIO_PIN0);   //enable



    GPIO_setOutputHighOnPin(GPIO_PORT_P8, GPIO_PIN2); //s1
    GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN5); //s0


    GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN0);


    // __delay_cycles(100000);

    // GPIO_setOutputHighOnPin(GPIO_PORT_P8, GPIO_PIN2); //s1
    // GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN5); //s0



}

void display_green_LED(){   //green is A1 01

    GPIO_setOutputHighOnPin(GPIO_PORT_P8, GPIO_PIN0);



    GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN2); //s1
    GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN5); //s0


    GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN0);


    // __delay_cycles(100000);

    // GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN2); //s1
    // GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN5); //s0


}

void display_red_LED(){   //green is A1 10
    GPIO_setOutputHighOnPin(GPIO_PORT_P8, GPIO_PIN0);



    GPIO_setOutputHighOnPin(GPIO_PORT_P8, GPIO_PIN2); //s1
    GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN5); //s0


    // __delay_cycles(100000);
    GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN0);

    // GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN2); //s1
    // GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN5); //s0


}

//Aarti functions start
double calculateDistance(int revs) {
    return (2*PI*RADIUS*revs/10);
}

void goLeft() {
    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN6);
    GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN0);
    GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN7);
    forwardKeypadPress = 1;
    while(!timeUp);
    return;
}

void goRight() {
    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);
    GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN7);
    GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN6);
    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN7);
    forwardKeypadPress = 1;
    while(!timeUp);
    return;
}

void RTC_ISR (void)
{
    switch (__even_in_range(RTCIV,2)){
        case 0: break;  //No interrupts
        case 2:         //RTC overflow
            timeUp = 1;
            break;
        default: break;
    }
}
//Aarti function ends


void main(void) {
    char buttonState = 0;
    /*
    * Functions with two underscores in front are called compiler intrinsics.
    * They are documented in the compiler user guide, not the IDE or MCU guides.
    * They are a shortcut to insert some assembly code that is not really
    * expressible in plain C/C++. Google "MSP430 Optimizing C/C++ Compiler
    * v18.12.0.LTS" and search for the word "intrinsic" if you want to know
    * more.
    *
    * */

    // Turn off interrupts during initialization

    __disable_interrupt();


    //Stop watchdog timer unless you plan on using it
    WDT_A_hold(WDT_A_BASE);

    // Initializations - see functions for more detail
    Init_GPIO();    //Sets all pins to output low as a default
    Init_PWM();     //Sets up a PWM output
    Init_ADC();     //Sets up the ADC to sample
    Init_Clock();   //Sets up the necessary system clocks
    Init_UART();    //Sets up an echo over a COM port
    Init_LCD();     //Sets up the LaunchPad LCD display

     /*
      * The MSP430 MCUs have a variety of low power modes. They can be almost
      * completely off and turn back on only when an interrupt occurs. You can
      * look up the power modes in the Family User Guide under the Power Management
      * Module (PMM) section. You can see the available API calls in the DriverLib
      * user guide, or see "pmm.h" in the driverlib directory. Unless you
      * purposefully want to play with the power modes, just leave this command in.
      */

    PMM_unlockLPM5(); //Disable the GPIO power-on default high-impedance mode to activate previously configured port settings

    //All done initializations - turn interrupts back on.
    __enable_interrupt();

    //Aarti demo code starts
    /*
    notes:
    1. Since U1 and U2 are close in proximity on the PCB, we can use them for wheel 1 and use U4, U5 for wheel 2
    */

    //Setting HEF GPIO pins as input:
    GPIO_setAsInputPin(GPIO_PORT_P8, GPIO_PIN1);//U2;J1-2 on schematic
    GPIO_setAsInputPin(GPIO_PORT_P1, GPIO_PIN1);//U1;J1-3 on schematic

    //KEYPAD GRID FORMATION

    //COLUMNS are set as inputs
    //ROWs are set as outputs
    //ROWs are set as high 1 by 1 and then they are sampled
    //when both row and column are high - number is displayed

    GPIO_setAsInputPin(GPIO_PORT_P1, GPIO_PIN4); //COL 1
    GPIO_setAsInputPin(GPIO_PORT_P8, GPIO_PIN3); //COL 2
    GPIO_setAsInputPin(GPIO_PORT_P5, GPIO_PIN3); //COL 3


    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN5); //ROW 1
    GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN0); //ROW 2
    GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN2); //ROW 3
    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN3); //ROW 4

    // for mux and in turn LEDS

    GPIO_setAsOutputPin(GPIO_PORT_P8, GPIO_PIN2); //for the mux - s1
    GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN5); //for mux - s0
    GPIO_setAsOutputPin(GPIO_PORT_P8, GPIO_PIN0); //for mux enable

    //init motor output pins
    GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN1);   //motor stdby
    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN6);
    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);
    GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN7);



    ////////////////////////////////////////////////////////////////////////////////


    char buttonState = 0;

    //init count variables for these inputs
    int U1Count = 0;
    int U2Count = 0;

    //init forward/backward flags for wheels 1 and 2
    int direction1[4] = {-1, -1, -1, -1};

    //init distance, speed vars
    //wheel 1
    double dist1 = 0;//in cm
    double speed1Initial = 0;//in cm/s - initial speed to compare speed1 with later on
    double speed1 = 0;

    //wheel 2
    double dist2 = 0;//in cm
    double speed2Initial = 0;//in cm/s - initial speed to compare speed2 with later on
    double speed2 = 0;

    //timer init - refer to RTC in MSP430FR4xx and MSP430FR2xx Family DriverLib User Guide in reference material
    //edit: refer to http://dev.ti.com/tirex/explore/node?node=APMDD8ahcwOHFDFCGMeZdw__IOGqZri__LATEST -> click import to CCS cloud ID

    //todo: may have to comment out some code in Init_Clock()
    //Set DCO FLL reference = REFO
    CS_initClockSignal(
        CS_FLLREF,
        CS_REFOCLK_SELECT,
        CS_CLOCK_DIVIDER_1
        );

    //Set SMCLK = REFO
    CS_initClockSignal(
        CS_SMCLK,
        CS_REFOCLK_SELECT,
        CS_CLOCK_DIVIDER_1
        );

    //Initialize RTC
    RTC_init(RTC_BASE,
        INTERVAL_TIME_INIT,
        RTC_CLOCKPREDIVIDER_1);

    RTC_clearInterrupt(RTC_BASE,
        RTC_OVERFLOW_INTERRUPT_FLAG);

    //Enable interrupt for RTC overflow
    RTC_enableInterrupt(RTC_BASE,
        RTC_OVERFLOW_INTERRUPT);

    int initial = 1;//for denoting break condition for initial RPM calculation

    LCD_welcome();

    LCD_keyInstructions();

    //poll for 5 secs
    int i = 0;
    for(i = 50000; i>0; i--)
    keypad_activate_dist(pos1, 0);

    //poll for 2 secs
//    int j = 0
    for(i=20000; i>0; i--)
    keypad_activate_dist(pos2, 1);

    for(i=20000; i>0; i--)
    keypad_activate_dist(pos3, 2);

    for(i=20000; i>0; i--)
    keypad_activate_dist(pos4, 3);

    //todo: fill this out
    distance = (distance_array[3]*1000) + (distance_array[2]*100) + (distance_array[1]*10) + distance_array[0];
    distanceFromKeypad = 1;

    //set threshold values
    int yellowThreshold = (0.5*distance);
    int redThreshold = (0.7*distance);

    //default dir is forward

    //busy wait for keypad to start
    while(!distanceFromKeypad);

    // //start timer - https://e2e.ti.com/support/microcontrollers/msp430/f/166/t/137303?What-does-ACLK-and-SMCLK-source- recommends ACLK
    RTC_start(RTC_BASE, RTC_CLOCKSOURCE_ACLK);

    //Start RTC Clock with clock source SMCLK
    RTC_start(RTC_BASE, RTC_CLOCKSOURCE_SMCLK);

    //Enter LPM3 mode with interrupts enabled
    __bis_SR_register(LPM0_bits + GIE);
    __no_operation();

    #if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
    #pragma vector=RTC_VECTOR
    __interrupt
    #elif defined(__GNUC__)
    __attribute__((interrupt(RTC_VECTOR)))
    #endif

    //set motors to forward on default
    GPIO_setOutputHighOnPin(GPIO_PORT_P5, GPIO_PIN1);
    GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN6);
    GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN0);
    GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN7);

//    int directionCheck = 1; //only do directionCheck once inside this loop
    while(initial && !timeUp) {
        //read HEF U1
        if(GPIO_getInputPinValue(GPIO_PORT_P8, GPIO_PIN1))
            U1Count++;
        if(GPIO_getInputPinValue(GPIO_PORT_P1, GPIO_PIN1))
            U2Count++;
        if (directionCheck) {
            direction1 = 0;
            todo: may have to check this
            todo: may run into issues if this loop is too slow for sensor inputs
            if (U1Count > U2Count) {
                direction1[FORWARD] = 1;//(say) forward direction
                direction1[BACKWARD] = 0;
            }
            else if (U2Count > U1Count) {
                direction1[BACKWARD] = 1;//(say) backward direction
                direction1[FORWARD] = 0;
            }
        }
    }

    timeUp = 0;
    initial = 0;

    dist1 = calculateDistance(U1Count);//in cm/s
    dist2 = calculateDistance(U2Count);
    speed1 = dist1*6;//in cm/s
    
    displayScrollText(speed1);

    display_green_LED();

    //stopping since we show that we're using hall effect sensors for distance feedback by displaying LEDs - just have to give feedback on direction
    RTC_stop(RTC_BASE);

    //initial display timer
    while(1) {
        //set output pins to high-low for both motors for default forward mode
        if (forwardKeypadPress) {
            GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN7);
            GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN6);
            GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN0);
            GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN7);
            forwardKeypadPress = 0;
        }

        //read HEF U1
        if(GPIO_getInputPinValue(GPIO_PORT_P8, GPIO_PIN1))
            U1Count++;
        //todo: need to test this to check if accurate
        //todo: may run into issues if this loop is too slow for sensor inputs
        if(GPIO_getInputPinValue(GPIO_PORT_P8, GPIO_PIN0))
            U2Count++;

        dist1 = calculateDistance(U1Count);//using U1 as HEF to calculate distance threshold

        //yellowThreshold is global var that's set to x% of distance after keypad first recieves distance
        if (dist1 == yellowThreshold) {
            //todo: turn led green off
            display_yellow_LED();
        }

        if (dist1 == redThreshold) {
            //todo: turn led yellow off
            display_red_LED();
        }

        if (dist1 == distance) {
            //todo: print DONE to LCD
            displayScrollText("WHEELCHAIR IS OUT OF POWER");
            break;
        }

        //leftKeypadPress = 1 when the appropriate key has been pressed on the keypad; so on for other presses
        if (leftKeypadPress) {
            RTC_setModulo(RTC_BASE, INTERVAL_TIME_LR);//todo: supposed to set timer to 0.1s - need to test to check if this is how it actually works
            RTC_start(RTC_BASE, RTC_CLOCKSOURCE_SMCLK);
            goLeft();//stop one motor and set high-low on other motor and return once 0.1s is up (since our motor is super fast)
            RTC_stop(RTC_BASE);
            leftKeypadPress = 0;
        }
        else if (rightKeypadPress) {
            RTC_setModulo(RTC_BASE, INTERVAL_TIME_LR);//todo: supposed to set timer to 0.1s - need to test to check if this is how it actually works
            RTC_start(RTC_BASE, RTC_CLOCKSOURCE_SMCLK);
            goRight();
            RTC_stop(RTC_BASE);
            rightKeypadPress = 0;
        }
        else if (backwardKeypadPress) {
            GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN6);
            GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN7);
            GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN7);
            GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);
            backwardKeypadPress = 0;
        }
    }
    //Aarti code ends
}


void Init_GPIO(void)
{
    // Set all GPIO pins to output low to prevent floating input and reduce power consumption
    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P3, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P4, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P6, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P7, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);

    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P3, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P4, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P6, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P7, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P8, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);

    //Set LaunchPad switches as inputs - they are active low, meaning '1' until pressed
    GPIO_setAsInputPinWithPullUpResistor(SW1_PORT, SW1_PIN);
    GPIO_setAsInputPinWithPullUpResistor(SW2_PORT, SW2_PIN);

    //Set LED1 and LED2 as outputs
    //GPIO_setAsOutputPin(LED1_PORT, LED1_PIN); //Comment if using UART
    GPIO_setAsOutputPin(LED2_PORT, LED2_PIN);
}

/* Clock System Initialization */
void Init_Clock(void)
{
    /*
     * The MSP430 has a number of different on-chip clocks. You can read about it in
     * the section of the Family User Guide regarding the Clock System ('cs.h' in the
     * driverlib).
     */

    /*
     * On the LaunchPad, there is a 32.768 kHz crystal oscillator used as a
     * Real Time Clock (RTC). It is a quartz crystal connected to a circuit that
     * resonates it. Since the frequency is a power of two, you can use the signal
     * to drive a counter, and you know that the bits represent binary fractions
     * of one second. You can then have the RTC module throw an interrupt based
     * on a 'real time'. E.g., you could have your system sleep until every
     * 100 ms when it wakes up and checks the status of a sensor. Or, you could
     * sample the ADC once per second.
     */
    //Set P4.1 and P4.2 as Primary Module Function Input, XT_LF
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P4, GPIO_PIN1 + GPIO_PIN2, GPIO_PRIMARY_MODULE_FUNCTION);

    // Set external clock frequency to 32.768 KHz
    CS_setExternalClockSource(32768);
    // Set ACLK = XT1
    CS_initClockSignal(CS_ACLK, CS_XT1CLK_SELECT, CS_CLOCK_DIVIDER_1);
    // Initializes the XT1 crystal oscillator
    CS_turnOnXT1LF(CS_XT1_DRIVE_1);
    // Set SMCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_SMCLK, CS_DCOCLKDIV_SELECT, CS_CLOCK_DIVIDER_1);
    // Set MCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_MCLK, CS_DCOCLKDIV_SELECT, CS_CLOCK_DIVIDER_1);
}

/* UART Initialization */
void Init_UART(void)
{
    /* UART: It configures P1.0 and P1.1 to be connected internally to the
     * eSCSI module, which is a serial communications module, and places it
     * in UART mode. This let's you communicate with the PC via a software
     * COM port over the USB cable. You can use a console program, like PuTTY,
     * to type to your LaunchPad. The code in this sample just echos back
     * whatever character was received.
     */

    //Configure UART pins, which maps them to a COM port over the USB cable
    //Set P1.0 and P1.1 as Secondary Module Function Input.
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1, GPIO_PIN1, GPIO_PRIMARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN0, GPIO_PRIMARY_MODULE_FUNCTION);

    /*
     * UART Configuration Parameter. These are the configuration parameters to
     * make the eUSCI A UART module to operate with a 9600 baud rate. These
     * values were calculated using the online calculator that TI provides at:
     * http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSP430BaudRateConverter/index.html
     */

    //SMCLK = 1MHz, Baudrate = 9600
    //UCBRx = 6, UCBRFx = 8, UCBRSx = 17, UCOS16 = 1
    EUSCI_A_UART_initParam param = {0};
        param.selectClockSource = EUSCI_A_UART_CLOCKSOURCE_SMCLK;
        param.clockPrescalar    = 6;
        param.firstModReg       = 8;
        param.secondModReg      = 17;
        param.parity            = EUSCI_A_UART_NO_PARITY;
        param.msborLsbFirst     = EUSCI_A_UART_LSB_FIRST;
        param.numberofStopBits  = EUSCI_A_UART_ONE_STOP_BIT;
        param.uartMode          = EUSCI_A_UART_MODE;
        param.overSampling      = 1;

    if(STATUS_FAIL == EUSCI_A_UART_init(EUSCI_A0_BASE, &param))
    {
        return;
    }

    EUSCI_A_UART_enable(EUSCI_A0_BASE);

    EUSCI_A_UART_clearInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);

    // Enable EUSCI_A0 RX interrupt
    EUSCI_A_UART_enableInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);
}

/* EUSCI A0 UART ISR - Echoes data back to PC host */
#pragma vector=USCI_A0_VECTOR
__interrupt
void EUSCIA0_ISR(void)
{
    uint8_t RxStatus = EUSCI_A_UART_getInterruptStatus(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG);

    EUSCI_A_UART_clearInterrupt(EUSCI_A0_BASE, RxStatus);

    if (RxStatus)
    {
        EUSCI_A_UART_transmitData(EUSCI_A0_BASE, EUSCI_A_UART_receiveData(EUSCI_A0_BASE));
    }
}

/* PWM Initialization */
void Init_PWM(void)
{
    /*
     * The internal timers (TIMER_A) can auto-generate a PWM signal without needing to
     * flip an output bit every cycle in software. The catch is that it limits which
     * pins you can use to output the signal, whereas manually flipping an output bit
     * means it can be on any GPIO. This function populates a data structure that tells
     * the API to use the timer as a hardware-generated PWM source.
     *
     */
    //Generate PWM - Timer runs in Up-Down mode
    param.clockSource           = TIMER_A_CLOCKSOURCE_SMCLK;
    param.clockSourceDivider    = TIMER_A_CLOCKSOURCE_DIVIDER_1;
    param.timerPeriod           = TIMER_A_PERIOD; //Defined in main.h
    param.compareRegister       = TIMER_A_CAPTURECOMPARE_REGISTER_1;
    param.compareOutputMode     = TIMER_A_OUTPUTMODE_RESET_SET;
    param.dutyCycle             = HIGH_COUNT; //Defined in main.h

    //PWM_PORT PWM_PIN (defined in main.h) as PWM output
    GPIO_setAsPeripheralModuleFunctionOutputPin(PWM_PORT, PWM_PIN, GPIO_PRIMARY_MODULE_FUNCTION);
}

void Init_ADC(void)
{
    /*
     * To use the ADC, you need to tell a physical pin to be an analog input instead
     * of a GPIO, then you need to tell the ADC to use that analog input. Defined
     * these in main.h for A9 on P8.1.
     */

    //Set ADC_IN to input direction
    GPIO_setAsPeripheralModuleFunctionInputPin(ADC_IN_PORT, ADC_IN_PIN, GPIO_PRIMARY_MODULE_FUNCTION);

    //Initialize the ADC Module
    /*
     * Base Address for the ADC Module
     * Use internal ADC bit as sample/hold signal to start conversion
     * USE MODOSC 5MHZ Digital Oscillator as clock source
     * Use default clock divider of 1
     */
    ADC_init(ADC_BASE,
             ADC_SAMPLEHOLDSOURCE_SC,
             ADC_CLOCKSOURCE_ADCOSC,
             ADC_CLOCKDIVIDER_1);

    ADC_enable(ADC_BASE);

    /*
     * Base Address for the ADC Module
     * Sample/hold for 16 clock cycles
     * Do not enable Multiple Sampling
     */
    ADC_setupSamplingTimer(ADC_BASE,
                           ADC_CYCLEHOLD_16_CYCLES,
                           ADC_MULTIPLESAMPLESDISABLE);

    //Configure Memory Buffer
    /*
     * Base Address for the ADC Module
     * Use input ADC_IN_CHANNEL
     * Use positive reference of AVcc
     * Use negative reference of AVss
     */
    ADC_configureMemory(ADC_BASE,
                        ADC_IN_CHANNEL,
                        ADC_VREFPOS_AVCC,
                        ADC_VREFNEG_AVSS);

    ADC_clearInterrupt(ADC_BASE,
                       ADC_COMPLETED_INTERRUPT);

    //Enable Memory Buffer interrupt
    ADC_enableInterrupt(ADC_BASE,
                        ADC_COMPLETED_INTERRUPT);
}

//ADC interrupt service routine
#pragma vector=ADC_VECTOR
__interrupt
void ADC_ISR(void)
{
    uint8_t ADCStatus = ADC_getInterruptStatus(ADC_BASE, ADC_COMPLETED_INTERRUPT_FLAG);

    ADC_clearInterrupt(ADC_BASE, ADCStatus);

    if (ADCStatus)
    {
        ADCState = 0; //Not busy anymore
        ADCResult = ADC_getResults(ADC_BASE);
    }
}

